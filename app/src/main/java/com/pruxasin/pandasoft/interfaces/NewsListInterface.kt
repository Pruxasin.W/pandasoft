package com.pruxasin.pandasoft.interfaces

import com.pruxasin.pandasoft.items.NewsItem

interface NewsListInterface {
    fun openNewsDetailActivity(newsItem: NewsItem)
    fun setNewsListToView(newsList: ArrayList<NewsItem>)
}