package com.pruxasin.pandasoft.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.pruxasin.pandasoft.R
import com.pruxasin.pandasoft.interfaces.NewsListInterface
import com.pruxasin.pandasoft.items.NewsItem
import kotlinx.android.synthetic.main.list_item_news.view.*

class NewsAdapter(
    context: Context,
    newsItemList: List<NewsItem>,
    newsListInterface: NewsListInterface
) :
    RecyclerView.Adapter<NewsAdapter.NewsViewHolder>() {

    private var mContext: Context = context
    private var mNewsItemList: List<NewsItem> = newsItemList
    private var mNewsListInterface = newsListInterface

    class NewsViewHolder(
        private val view: View,
        private val context: Context,
        private val newsListInterface: NewsListInterface
    ) : RecyclerView.ViewHolder(view) {

        fun bindView(newsItem: NewsItem) {
            Glide.with(context).load(newsItem.image).into(view.list_item_news_image_view_news_image)

            view.list_item_news_text_view_news_title.text = newsItem.title
            view.list_item_news_linear_layout_root.setOnClickListener {
                newsListInterface.openNewsDetailActivity(newsItem)
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): NewsAdapter.NewsViewHolder {
        val inflatedView = parent.inflate(R.layout.list_item_news, false)
        return NewsViewHolder(inflatedView, mContext, mNewsListInterface)
    }

    override fun getItemCount(): Int = mNewsItemList.size

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        val newsItem: NewsItem = mNewsItemList.get(position)
        holder.bindView(newsItem)
    }

    private fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
        return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
    }
}