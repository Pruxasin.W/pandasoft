package com.pruxasin.pandasoft.utils

import android.content.Context
import androidx.preference.PreferenceManager

class PandasoftSharedPreferences(context: Context) {

    private var mContext = context
    private var mInstance: PandasoftSharedPreferences? = null
    private var mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    fun getInstance(): PandasoftSharedPreferences? {
        if (mInstance == null) {
            mInstance = PandasoftSharedPreferences(mContext.applicationContext)
        }
        return mInstance
    }

    fun setAccessToken(accessToken: String?) {
        mSharedPreferences.edit().putString(Keys.ACCESS_TOKEN, accessToken).apply()
    }

    fun getAccessToken(): String? {
        return mSharedPreferences.getString(Keys.ACCESS_TOKEN, "")
    }

    fun setRefreshToken(refreshToken: String?) {
        mSharedPreferences.edit().putString(Keys.REFRESH_TOKEN, refreshToken).apply()
    }

    fun getRefreshToken(): String? {
        return mSharedPreferences.getString(Keys.REFRESH_TOKEN, "")
    }

    fun setExpiresIn(expiresIn: Int?) {
        mSharedPreferences.edit().putInt(Keys.EXPIRES_IN, expiresIn!!).apply()
    }

    fun getExpiresIn(): Int? {
        return mSharedPreferences.getInt(Keys.EXPIRES_IN, 640)
    }

/*    fun setAccessTokenTimeStamp(timeStamp: Long?) {
        mSharedPreferences.edit().putLong(Keys.ACCESS_TOKEN_TIME_STAMP, timeStamp!!).apply()
    }

    fun getAccessTokenTimeStamp(): Long? {
        return mSharedPreferences.getLong(Keys.ACCESS_TOKEN_TIME_STAMP, 0L)
    }*/

    fun setAccessTokenExpiresIn(expiresIn: Long?) {
        mSharedPreferences.edit().putLong(Keys.ACCESS_TOKEN_EXPIRES_IN, expiresIn!!).apply()
    }

    fun getAccessTokenExpiresIn(): Long? {
        return mSharedPreferences.getLong(Keys.ACCESS_TOKEN_EXPIRES_IN, 0L)
    }
}