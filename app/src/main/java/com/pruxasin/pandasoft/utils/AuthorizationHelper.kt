package com.pruxasin.pandasoft.utils

import android.content.Context
import android.util.Log
import com.pruxasin.pandasoft.apis.RefreshTokenApi

class AuthorizationHelper(context: Context) {

    private var mContext = context
    private var mSharedPreferences: PandasoftSharedPreferences? = null

    private fun isTokenExpired(): Boolean {
        mSharedPreferences = PandasoftSharedPreferences(mContext).getInstance()

        val currentTime: Long? = Util().convertDateToLong(Util().getCalendar().time.toString())
        val expiredTime: Long? = mSharedPreferences?.getAccessTokenExpiresIn()

        Log.d(Keys.TAG, "current time = $currentTime, token expired time = $expiredTime")

        return currentTime!! - expiredTime!! >= 0
    }

    fun refreshToken() {
        if (isTokenExpired()) {
            val refreshTokenApi = RefreshTokenApi(mContext)
            refreshTokenApi.doRefreshToken()
        }
    }
}