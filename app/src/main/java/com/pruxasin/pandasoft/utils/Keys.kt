package com.pruxasin.pandasoft.utils

object Keys {
    const val TAG = "_pandasoft"
    const val ACCESS_TOKEN = "access_token"
    const val REFRESH_TOKEN = "refresh_token"
    const val EXPIRES_IN = "expires_in"
    const val NEWS_DETAIL = "new_detail"
    const val ACCESS_TOKEN_TIME_STAMP = "access_token_time_stamp"
    const val ACCESS_TOKEN_EXPIRES_IN = "access_token_expires_in"
    const val IDLE_TIME = 600000L
    const val REQUEST_FINISH_ACTIVITY = 201
}