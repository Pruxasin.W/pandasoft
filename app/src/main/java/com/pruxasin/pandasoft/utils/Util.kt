package com.pruxasin.pandasoft.utils

import org.threeten.bp.OffsetDateTime
import org.threeten.bp.format.DateTimeFormatter
import java.util.*

class Util {
    fun convertDateToLong(dateString: String): Long? {
        val formatter: DateTimeFormatter =
            DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss OOOO yyyy", Locale.ROOT)
        return OffsetDateTime.parse(dateString, formatter)
            .toInstant()
            .toEpochMilli()
    }

    fun getCalendar(): Calendar {
        return Calendar.getInstance(Locale("th", "th"))
    }
}