package com.pruxasin.pandasoft.items

import android.os.Parcel
import android.os.Parcelable

class NewsItem() : Parcelable {
    var id: String? = ""
    var uuid: String? = ""
    var create: String? = ""
    var title: String? = ""
    var image: String? = ""
    var detail: String? = ""

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        uuid = parcel.readString()
        create = parcel.readString()
        title = parcel.readString()
        image = parcel.readString()
        detail = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(uuid)
        parcel.writeString(create)
        parcel.writeString(title)
        parcel.writeString(image)
        parcel.writeString(detail)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<NewsItem> {
        override fun createFromParcel(parcel: Parcel): NewsItem {
            return NewsItem(parcel)
        }

        override fun newArray(size: Int): Array<NewsItem?> {
            return arrayOfNulls(size)
        }
    }
}