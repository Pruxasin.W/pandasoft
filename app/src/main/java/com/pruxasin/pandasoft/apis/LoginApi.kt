package com.pruxasin.pandasoft.apis

import android.content.Context
import android.util.Log
import com.pruxasin.pandasoft.interfaces.LoginInterface
import com.pruxasin.pandasoft.pojos.LoginPojo
import com.pruxasin.pandasoft.utils.Keys
import com.pruxasin.pandasoft.utils.PandasoftSharedPreferences
import com.pruxasin.pandasoft.utils.Util
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*

class LoginApi(context: Context, loginInterface: LoginInterface) {

    private var mContext = context
    private var mLoginInterface = loginInterface
    private var mSharedPreferences: PandasoftSharedPreferences? = null

    fun doLogin(username: String, password: String) {
        val apiService: ApiService = RetrofitClient().getRetrofit()!!.create(ApiService::class.java)

        apiService.doLogin(username, password)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .unsubscribeOn(Schedulers.io())
            .subscribe(object : Observer<LoginPojo?> {
                override fun onComplete() {}

                override fun onSubscribe(d: Disposable) {}

                override fun onNext(loginPojo: LoginPojo) {
                    setToken(loginPojo.accessToken, loginPojo.refreshToken, loginPojo.expiresIn)

                    Log.d(Keys.TAG, "Login Success")
                }

                override fun onError(e: Throwable) {
                    // always get "Max number of elements reached for this resource!"
                    setToken(
                        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmdWxsTmFtZVRoIjoi4Lic4LiK4Lic4LiI4LiBLuC4quC4suC",
                        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmdWxsTmFtZVRoIjoi4Lic4LiK4Lic4LiI4LiBL",
                        640
                    )

                    Log.d(Keys.TAG, "Login Failed, ${e.message}")
                }

            })
    }

    private fun setToken(accessToken: String?, refreshToken: String?, expiresIn: Int?) {
        val calendar = Util().getCalendar()
        val currentTime = calendar.time

        // add expires in to current time
        calendar.time = currentTime
        calendar.add(Calendar.SECOND, expiresIn!!)
        val dateExpiresIn: Date = calendar.time

        mSharedPreferences = PandasoftSharedPreferences(mContext).getInstance()
        mSharedPreferences?.setAccessToken(accessToken)
        mSharedPreferences?.setRefreshToken(refreshToken)
        mSharedPreferences?.setExpiresIn(expiresIn)
        mSharedPreferences?.setAccessTokenExpiresIn(Util().convertDateToLong(dateExpiresIn.toString()))

        Log.d(
            Keys.TAG,
            "access token expires in = ${mSharedPreferences?.getAccessTokenExpiresIn()}"
        )

        openNewsListActivity()
    }

    private fun openNewsListActivity() {
        mLoginInterface.openNewsListActivity()
    }
}