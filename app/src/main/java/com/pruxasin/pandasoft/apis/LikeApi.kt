package com.pruxasin.pandasoft.apis

import android.content.Context
import android.util.Log
import com.pruxasin.pandasoft.interfaces.LikeInterface
import com.pruxasin.pandasoft.pojos.LikePojo
import com.pruxasin.pandasoft.utils.Keys
import com.pruxasin.pandasoft.utils.PandasoftSharedPreferences
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class LikeApi(context: Context, likeInterface: LikeInterface) {

    private var mContext = context
    private var mLikeInterface = likeInterface
    private var mSharedPreferences: PandasoftSharedPreferences? = null

    fun doLike(newsId: String) {
        val apiService: ApiService = RetrofitClient().getRetrofit()!!.create(ApiService::class.java)

        apiService.doLike(newsId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .unsubscribeOn(Schedulers.io())
            .subscribe(object : Observer<LikePojo?> {
                override fun onComplete() {

                }

                override fun onSubscribe(d: Disposable) {
                }

                override fun onNext(likePojo: LikePojo) {
                    Log.d(Keys.TAG, "do like newsId = $newsId success")
                    updateLikeButtonUi()
                }

                override fun onError(e: Throwable) {
                    // always get "Max number of elements reached for this resource!"
                    Log.d(Keys.TAG, "do like newsId = $newsId failed")
                    updateLikeButtonUi()
                }
            })
    }

    private fun updateLikeButtonUi() {
        mLikeInterface.updateLikeButtonUi()
    }
}