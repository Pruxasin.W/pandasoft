package com.pruxasin.pandasoft.apis

import com.pruxasin.pandasoft.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitClient {
    private var mRetrofit: Retrofit? = null
    private val mBaseUrl: String = "https://5c065a3fc16e1200139479cc.mockapi.io/"

    fun getRetrofit(): Retrofit? {
        val logging = HttpLoggingInterceptor()
        logging.apply {
            level =
                if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        }

        val client: OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(logging)
            .build()

        mRetrofit = Retrofit.Builder()
            .baseUrl(mBaseUrl)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

        return mRetrofit
    }
}