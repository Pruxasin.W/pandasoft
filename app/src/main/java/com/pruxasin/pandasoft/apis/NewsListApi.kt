package com.pruxasin.pandasoft.apis

import android.content.Context
import android.util.Log
import com.pruxasin.pandasoft.interfaces.NewsListInterface
import com.pruxasin.pandasoft.items.NewsItem
import com.pruxasin.pandasoft.pojos.NewsListPojo
import com.pruxasin.pandasoft.utils.Keys
import com.pruxasin.pandasoft.utils.PandasoftSharedPreferences
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class NewsListApi(context: Context, newsListInterface: NewsListInterface) {

    private var mContext = context
    private var mNewsListInterface = newsListInterface
    private var mSharedPreferences: PandasoftSharedPreferences? = null

    fun doGetNewsList() {
        val apiService: ApiService = RetrofitClient().getRetrofit()!!.create(ApiService::class.java)

        apiService.doGetNewsList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .unsubscribeOn(Schedulers.io())
            .subscribe(object : Observer<NewsListPojo?> {
                override fun onComplete() {

                }

                override fun onSubscribe(d: Disposable) {
                }

                override fun onNext(newsListPojo: NewsListPojo) {
                    val newsList: ArrayList<NewsItem> = ArrayList()

                    val datas: List<NewsListPojo.Datum>? = newsListPojo.data

                    Log.d(Keys.TAG, "Load News list success, data size = ${datas?.size}")

                    for (data in datas!!) {
                        val newsItem = NewsItem()
                        newsItem.id = data.id
                        newsItem.uuid = data.uuid
                        newsItem.create = data.create
                        newsItem.title = data.title
                        newsItem.image = data.image
                        newsItem.detail = data.detail

                        newsList.add(newsItem)
                    }

                    setNewsListToView(newsList)
                }

                override fun onError(e: Throwable) {
                    Log.d(Keys.TAG, "onError News List, ${e.message}")
                }
            })
    }

    private fun setNewsListToView(newsList: ArrayList<NewsItem>) {
        mNewsListInterface.setNewsListToView(newsList)
    }
}