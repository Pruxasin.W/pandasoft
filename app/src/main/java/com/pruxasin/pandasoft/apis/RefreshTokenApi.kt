package com.pruxasin.pandasoft.apis

import android.content.Context
import android.util.Log
import com.pruxasin.pandasoft.pojos.RefreshTokenPojo
import com.pruxasin.pandasoft.utils.Keys
import com.pruxasin.pandasoft.utils.PandasoftSharedPreferences
import com.pruxasin.pandasoft.utils.Util
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*

class RefreshTokenApi(context: Context) {

    private var mContext = context
    private var mSharedPreferences: PandasoftSharedPreferences? = null

    fun doRefreshToken() {
        val apiService: ApiService = RetrofitClient().getRetrofit()!!.create(ApiService::class.java)

        mSharedPreferences = PandasoftSharedPreferences(mContext).getInstance()
        val refreshToken = mSharedPreferences?.getRefreshToken()

        apiService.doRefreshToken(refreshToken!!)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .unsubscribeOn(Schedulers.io())
            .subscribe(object : Observer<RefreshTokenPojo?> {
                override fun onComplete() {

                }

                override fun onSubscribe(d: Disposable) {
                }

                override fun onNext(refreshTokenPojo: RefreshTokenPojo) {
                    setNewToken(
                        refreshTokenPojo.accessToken,
                        refreshTokenPojo.refreshToken,
                        refreshTokenPojo.expiresIn
                    )

                    Log.d(Keys.TAG, "Refresh token success")
                }

                override fun onError(e: Throwable) {
                    Log.d(Keys.TAG, "Refresh token failed, ${e.message}")

                }
            })
    }

    private fun setNewToken(accessToken: String?, refreshToken: String?, expiresIn: Int?) {
        val calendar = Util().getCalendar()
        val currentTime = calendar.time

        // add 'expires_in' to current time
        calendar.time = currentTime
        calendar.add(Calendar.SECOND, expiresIn!!)
        val dateExpiresIn: Date = calendar.time

        mSharedPreferences = PandasoftSharedPreferences(mContext).getInstance()
        mSharedPreferences?.setAccessToken(accessToken)
        mSharedPreferences?.setRefreshToken(refreshToken)
        mSharedPreferences?.setExpiresIn(expiresIn)
        mSharedPreferences?.setAccessTokenExpiresIn(Util().convertDateToLong(dateExpiresIn.toString()))

        Log.d(
            Keys.TAG,
            "new access token expires in = ${mSharedPreferences?.getAccessTokenExpiresIn()}"
        )
    }
}