package com.pruxasin.pandasoft.apis

import com.pruxasin.pandasoft.pojos.LikePojo
import com.pruxasin.pandasoft.pojos.LoginPojo
import com.pruxasin.pandasoft.pojos.NewsListPojo
import com.pruxasin.pandasoft.pojos.RefreshTokenPojo
import io.reactivex.Observable
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiService {
    @POST("api/v1/login")
    @FormUrlEncoded
    fun doLogin(@Field("username") username: String, @Field("password") password: String): Observable<LoginPojo>

    @GET("api/v1/news")
    fun doGetNewsList(): Observable<NewsListPojo>

    @POST("api/v1/like")
    @FormUrlEncoded
    fun doLike(@Field("newsId") newsId: String): Observable<LikePojo>

    @POST("api/v1/refresh")
    @FormUrlEncoded
    fun doRefreshToken(@Field("refresh_token") refreshToken: String): Observable<RefreshTokenPojo>
}