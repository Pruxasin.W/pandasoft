package com.pruxasin.pandasoft.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.pruxasin.pandasoft.R
import com.pruxasin.pandasoft.apis.LikeApi
import com.pruxasin.pandasoft.interfaces.LikeInterface
import com.pruxasin.pandasoft.items.NewsItem
import com.pruxasin.pandasoft.utils.AuthorizationHelper
import com.pruxasin.pandasoft.utils.Keys
import com.pruxasin.pandasoft.utils.PandasoftSharedPreferences
import kotlinx.android.synthetic.main.activity_news_detail.*
import java.text.SimpleDateFormat
import java.util.*

class NewsDetailActivity : AppCompatActivity(), LikeInterface {

    private lateinit var mContext: Context
    private var mNewsItem: NewsItem? = null
    private var mSharedPreferences: PandasoftSharedPreferences? = null
    private var mIdleHandler: Handler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_detail)

        mContext = this

        mNewsItem = intent.getParcelableExtra(Keys.NEWS_DETAIL)
        val newsItem = mNewsItem

        setDataToView(newsItem)

        activity_news_detail_linear_layout_like_button.setOnClickListener {
            checkIfTokenExpired()

            val likeApi = LikeApi(mContext, this)
            likeApi.doLike(newsItem?.id!!)
        }
    }

    override fun onResume() {
        super.onResume()
        delayedIdle()
        checkIfTokenExpired()
    }

    private fun checkIfTokenExpired() {
        AuthorizationHelper(mContext).refreshToken()
    }

    private fun setDataToView(data: NewsItem?) {
        Glide.with(this).load(data?.image).into(activity_news_detail_image_view_image)

        activity_news_detail_text_view_title.text = data?.title
        activity_news_detail_text_view_detail.text = data?.detail

        activity_news_detail_text_view_date.text = convertLongToDate(data?.create?.toLongOrNull())
    }

    private fun convertLongToDate(create: Long?): String {
        return try {
            val netDate = Date(create?.times(1000)!!)
            val format = SimpleDateFormat("dd/MM/yyyy HH:mm")
            format.format(netDate)
        } catch (e: Exception) {
            e.toString()
        }
    }

    override fun updateLikeButtonUi() {
        val likeStatus = activity_news_detail_text_view_like.text.toString()
        if (likeStatus == getString(R.string.like)) {
            activity_news_detail_text_view_like.text = getString(R.string.liked)
        } else {
            activity_news_detail_text_view_like.text = getString(R.string.like)
        }
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        delayedIdle()
    }

    private var mIdleRunnable = Runnable {
        mSharedPreferences = PandasoftSharedPreferences(this).getInstance()
        mSharedPreferences?.setAccessToken("")
        mSharedPreferences?.setAccessTokenExpiresIn(0L)
        mSharedPreferences?.setRefreshToken("")
        mSharedPreferences?.setExpiresIn(0)

        setResult(Activity.RESULT_OK, null)
        this.finish()

        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)

        Log.d(Keys.TAG, "user idle")

    }

    private fun delayedIdle() {
        mIdleHandler.removeCallbacks(mIdleRunnable)
        mIdleHandler.postDelayed(mIdleRunnable, Keys.IDLE_TIME)
    }

    override fun onPause() {
        super.onPause()
        mIdleHandler.removeCallbacks(mIdleRunnable)
    }
}
