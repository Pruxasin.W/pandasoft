package com.pruxasin.pandasoft.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.pruxasin.pandasoft.R
import com.pruxasin.pandasoft.apis.LoginApi
import com.pruxasin.pandasoft.interfaces.LoginInterface
import com.pruxasin.pandasoft.utils.AuthorizationHelper
import com.pruxasin.pandasoft.utils.PandasoftSharedPreferences
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), LoginInterface {

    private lateinit var mContext: Context
    private var mUsername = ""
    private var mPassword = ""
    private var mSharedPreferences: PandasoftSharedPreferences? = null
    private lateinit var mAccessToken: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        mContext = this

        mSharedPreferences = PandasoftSharedPreferences(mContext).getInstance()

        checkIfHasToken()

        activity_login_login_button.setOnClickListener(loginButtonOnClickedListener)
    }

    override fun onResume() {
        super.onResume()
        checkIfTokenExpired()
    }

    private fun checkIfTokenExpired() {
        if (mAccessToken != "") {
            AuthorizationHelper(mContext).refreshToken()
        }
    }

    private fun checkIfHasToken() {
        mAccessToken = mSharedPreferences?.getAccessToken()!!

        if (mAccessToken != "") {
            checkIfTokenExpired()

            openNewsListActivity()
        }
    }

    private val loginButtonOnClickedListener = View.OnClickListener {
        mUsername = activity_login_edit_text_username.text.toString()
        mPassword = activity_login_edit_text_password.text.toString()

        if (mUsername.isNotEmpty() && mPassword.isNotEmpty()) {
            val loginApi = LoginApi(mContext, this)
            loginApi.doLogin(mUsername, mPassword)
        } else {
            Toast.makeText(mContext, "Please enter all fields", Toast.LENGTH_SHORT).show()
        }
//        Log.d(Keys.TAG, "username = $mUsername, password = $mPassword")
    }

    override fun openNewsListActivity() {
        val intent = Intent(this, NewsListActivity::class.java)
        startActivity(intent)
        this.finish()
    }
}
