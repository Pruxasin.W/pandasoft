package com.pruxasin.pandasoft.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.pruxasin.pandasoft.R
import com.pruxasin.pandasoft.adapters.NewsAdapter
import com.pruxasin.pandasoft.apis.NewsListApi
import com.pruxasin.pandasoft.interfaces.NewsListInterface
import com.pruxasin.pandasoft.items.NewsItem
import com.pruxasin.pandasoft.utils.AuthorizationHelper
import com.pruxasin.pandasoft.utils.Keys
import com.pruxasin.pandasoft.utils.PandasoftSharedPreferences
import kotlinx.android.synthetic.main.activity_news_list.*

class NewsListActivity : AppCompatActivity(), NewsListInterface {

    private lateinit var mContext: Context
    private lateinit var mNewsList: ArrayList<NewsItem>
    private lateinit var mLinearLayoutManager: LinearLayoutManager
    private lateinit var mNewsAdapter: NewsAdapter
    private var mSharedPreferences: PandasoftSharedPreferences? = null
    private var mIdleHandler: Handler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_list)

        mContext = this

        fetchNewsList()
    }

    override fun onResume() {
        super.onResume()
        delayedIdle()
        checkIfTokenExpired()
    }

    private fun checkIfTokenExpired() {
        AuthorizationHelper(mContext).refreshToken()
    }

    private fun fetchNewsList() {
        checkIfTokenExpired()

        val newsListApi = NewsListApi(mContext, this)
        newsListApi.doGetNewsList()
    }

    override fun openNewsDetailActivity(newsItem: NewsItem) {
        val intent = Intent(this, NewsDetailActivity::class.java)
        intent.putExtra(Keys.NEWS_DETAIL, newsItem)
        startActivityForResult(intent, Keys.REQUEST_FINISH_ACTIVITY)
    }

    override fun setNewsListToView(newsList: ArrayList<NewsItem>) {
        mNewsList = newsList

        mLinearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
        activity_news_recycler_view_news_list.layoutManager = mLinearLayoutManager
        mNewsAdapter = NewsAdapter(mContext, mNewsList, this)
        activity_news_recycler_view_news_list.adapter = mNewsAdapter
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        delayedIdle()
    }

    private var mIdleRunnable = Runnable {
        mSharedPreferences = PandasoftSharedPreferences(this).getInstance()
        mSharedPreferences?.setAccessToken("")
        mSharedPreferences?.setAccessTokenExpiresIn(0L)
        mSharedPreferences?.setRefreshToken("")
        mSharedPreferences?.setExpiresIn(0)

        this.finish()

        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)

        Log.d(Keys.TAG, "user idle")

    }

    private fun delayedIdle() {
        mIdleHandler.removeCallbacks(mIdleRunnable)
        mIdleHandler.postDelayed(mIdleRunnable, Keys.IDLE_TIME)
    }

    override fun onPause() {
        super.onPause()
        mIdleHandler.removeCallbacks(mIdleRunnable)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Keys.REQUEST_FINISH_ACTIVITY) {
            if (resultCode == RESULT_OK) {
                this.finish()
            }
        }
    }
}
