package com.pruxasin.pandasoft.pojos

import com.google.gson.annotations.SerializedName

class RefreshTokenPojo {
    @SerializedName("status")
    var status: Int? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: Any? = null

    @SerializedName("access_token")
    var accessToken: String? = null

    @SerializedName("refresh_token")
    var refreshToken: String? = null

    @SerializedName("expires_in")
    var expiresIn: Int? = null
}