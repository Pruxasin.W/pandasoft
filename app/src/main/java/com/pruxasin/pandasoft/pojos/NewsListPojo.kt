package com.pruxasin.pandasoft.pojos

import com.google.gson.annotations.SerializedName

class NewsListPojo {
    @SerializedName("status")
    var status: Int? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: List<Datum>? = null

    class Datum {
        @SerializedName("id")
        var id: String? = null

        @SerializedName("uuid")
        var uuid: String? = null

        @SerializedName("create")
        var create: String? = null

        @SerializedName("title")
        var title: String? = null

        @SerializedName("image")
        var image: String? = null

        @SerializedName("detail")
        var detail: String? = null
    }
}