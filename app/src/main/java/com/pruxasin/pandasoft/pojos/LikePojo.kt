package com.pruxasin.pandasoft.pojos

import com.google.gson.annotations.SerializedName

class LikePojo {
    @SerializedName("status")
    var status: Int? = null

    @SerializedName("message")
    var message: String? = null
}